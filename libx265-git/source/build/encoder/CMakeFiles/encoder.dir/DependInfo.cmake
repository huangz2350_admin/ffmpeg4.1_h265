# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/share/Media/libx265-git/source/encoder/analysis.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/analysis.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/api.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/api.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/bitcost.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/bitcost.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/dpb.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/dpb.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/encoder.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/encoder.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/entropy.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/entropy.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/frameencoder.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/frameencoder.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/framefilter.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/framefilter.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/level.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/level.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/motion.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/motion.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/nal.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/nal.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/ratecontrol.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/ratecontrol.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/reference.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/reference.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/sao.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/sao.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/search.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/search.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/sei.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/sei.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/slicetype.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/slicetype.cpp.o"
  "/root/share/Media/libx265-git/source/encoder/weightPrediction.cpp" "/root/share/Media/libx265-git/source/build/encoder/CMakeFiles/encoder.dir/weightPrediction.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "EXPORT_C_API=1"
  "HAVE_INT_TYPES_H=1"
  "HIGH_BIT_DEPTH=0"
  "X265_ARCH_X86=1"
  "X265_DEPTH=8"
  "X265_NS=x265"
  "X86_64=1"
  "__STDC_LIMIT_MACROS=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../."
  "../common"
  "../encoder"
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
