# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/share/Media/libx265-git/source/input/input.cpp" "/root/share/Media/libx265-git/source/build/CMakeFiles/cli.dir/input/input.cpp.o"
  "/root/share/Media/libx265-git/source/input/y4m.cpp" "/root/share/Media/libx265-git/source/build/CMakeFiles/cli.dir/input/y4m.cpp.o"
  "/root/share/Media/libx265-git/source/input/yuv.cpp" "/root/share/Media/libx265-git/source/build/CMakeFiles/cli.dir/input/yuv.cpp.o"
  "/root/share/Media/libx265-git/source/output/output.cpp" "/root/share/Media/libx265-git/source/build/CMakeFiles/cli.dir/output/output.cpp.o"
  "/root/share/Media/libx265-git/source/output/raw.cpp" "/root/share/Media/libx265-git/source/build/CMakeFiles/cli.dir/output/raw.cpp.o"
  "/root/share/Media/libx265-git/source/output/reconplay.cpp" "/root/share/Media/libx265-git/source/build/CMakeFiles/cli.dir/output/reconplay.cpp.o"
  "/root/share/Media/libx265-git/source/output/y4m.cpp" "/root/share/Media/libx265-git/source/build/CMakeFiles/cli.dir/output/y4m.cpp.o"
  "/root/share/Media/libx265-git/source/output/yuv.cpp" "/root/share/Media/libx265-git/source/build/CMakeFiles/cli.dir/output/yuv.cpp.o"
  "/root/share/Media/libx265-git/source/x265-extras.cpp" "/root/share/Media/libx265-git/source/build/CMakeFiles/cli.dir/x265-extras.cpp.o"
  "/root/share/Media/libx265-git/source/x265.cpp" "/root/share/Media/libx265-git/source/build/CMakeFiles/cli.dir/x265.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "EXPORT_C_API=1"
  "HAVE_INT_TYPES_H=1"
  "HIGH_BIT_DEPTH=0"
  "X265_ARCH_X86=1"
  "X265_DEPTH=8"
  "X265_NS=x265"
  "X86_64=1"
  "__STDC_LIMIT_MACROS=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../."
  "../common"
  "../encoder"
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/root/share/Media/libx265-git/source/build/CMakeFiles/x265-shared.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
