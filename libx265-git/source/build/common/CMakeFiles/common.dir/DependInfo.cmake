# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/share/Media/libx265-git/source/common/bitstream.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/bitstream.cpp.o"
  "/root/share/Media/libx265-git/source/common/common.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/common.cpp.o"
  "/root/share/Media/libx265-git/source/common/constants.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/constants.cpp.o"
  "/root/share/Media/libx265-git/source/common/cpu.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/cpu.cpp.o"
  "/root/share/Media/libx265-git/source/common/cudata.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/cudata.cpp.o"
  "/root/share/Media/libx265-git/source/common/dct.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/dct.cpp.o"
  "/root/share/Media/libx265-git/source/common/deblock.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/deblock.cpp.o"
  "/root/share/Media/libx265-git/source/common/frame.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/frame.cpp.o"
  "/root/share/Media/libx265-git/source/common/framedata.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/framedata.cpp.o"
  "/root/share/Media/libx265-git/source/common/intrapred.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/intrapred.cpp.o"
  "/root/share/Media/libx265-git/source/common/ipfilter.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/ipfilter.cpp.o"
  "/root/share/Media/libx265-git/source/common/loopfilter.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/loopfilter.cpp.o"
  "/root/share/Media/libx265-git/source/common/lowres.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/lowres.cpp.o"
  "/root/share/Media/libx265-git/source/common/md5.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/md5.cpp.o"
  "/root/share/Media/libx265-git/source/common/param.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/param.cpp.o"
  "/root/share/Media/libx265-git/source/common/piclist.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/piclist.cpp.o"
  "/root/share/Media/libx265-git/source/common/picyuv.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/picyuv.cpp.o"
  "/root/share/Media/libx265-git/source/common/pixel.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/pixel.cpp.o"
  "/root/share/Media/libx265-git/source/common/predict.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/predict.cpp.o"
  "/root/share/Media/libx265-git/source/common/primitives.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/primitives.cpp.o"
  "/root/share/Media/libx265-git/source/common/quant.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/quant.cpp.o"
  "/root/share/Media/libx265-git/source/common/scalinglist.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/scalinglist.cpp.o"
  "/root/share/Media/libx265-git/source/common/shortyuv.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/shortyuv.cpp.o"
  "/root/share/Media/libx265-git/source/common/slice.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/slice.cpp.o"
  "/root/share/Media/libx265-git/source/common/threading.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/threading.cpp.o"
  "/root/share/Media/libx265-git/source/common/threadpool.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/threadpool.cpp.o"
  "/root/share/Media/libx265-git/source/common/version.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/version.cpp.o"
  "/root/share/Media/libx265-git/source/common/wavefront.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/wavefront.cpp.o"
  "/root/share/Media/libx265-git/source/common/yuv.cpp" "/root/share/Media/libx265-git/source/build/common/CMakeFiles/common.dir/yuv.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "EXPORT_C_API=1"
  "HAVE_INT_TYPES_H=1"
  "HIGH_BIT_DEPTH=0"
  "X265_ARCH_X86=1"
  "X265_DEPTH=8"
  "X265_NS=x265"
  "X86_64=1"
  "__STDC_LIMIT_MACROS=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../."
  "../common"
  "../encoder"
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
