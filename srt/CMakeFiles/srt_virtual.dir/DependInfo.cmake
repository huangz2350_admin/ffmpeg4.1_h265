# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/root/share/Media/srt/haicrypt/cryspr-openssl.c" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/haicrypt/cryspr-openssl.c.o"
  "/root/share/Media/srt/haicrypt/cryspr.c" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/haicrypt/cryspr.c.o"
  "/root/share/Media/srt/haicrypt/hcrypt.c" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/haicrypt/hcrypt.c.o"
  "/root/share/Media/srt/haicrypt/hcrypt_ctx_rx.c" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/haicrypt/hcrypt_ctx_rx.c.o"
  "/root/share/Media/srt/haicrypt/hcrypt_ctx_tx.c" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/haicrypt/hcrypt_ctx_tx.c.o"
  "/root/share/Media/srt/haicrypt/hcrypt_rx.c" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/haicrypt/hcrypt_rx.c.o"
  "/root/share/Media/srt/haicrypt/hcrypt_sa.c" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/haicrypt/hcrypt_sa.c.o"
  "/root/share/Media/srt/haicrypt/hcrypt_tx.c" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/haicrypt/hcrypt_tx.c.o"
  "/root/share/Media/srt/haicrypt/hcrypt_xpt_srt.c" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/haicrypt/hcrypt_xpt_srt.c.o"
  "/root/share/Media/srt/srtcore/srt_compat.c" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/srt_compat.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "HAI_ENABLE_SRT=1"
  "HAI_PATCH=1"
  "HAVE_INET_PTON=1"
  "LINUX=1"
  "SRT_DYNAMIC"
  "SRT_ENABLE_APP_READER"
  "SRT_ENABLE_ENCRYPTION"
  "SRT_EXPORTS"
  "SRT_VERSION=\"1.4.1\""
  "USE_OPENSSL=1"
  "_GNU_SOURCE"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "common"
  "srtcore"
  "haicrypt"
  "."
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/share/Media/srt/haicrypt/haicrypt_log.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/haicrypt/haicrypt_log.cpp.o"
  "/root/share/Media/srt/srtcore/api.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/api.cpp.o"
  "/root/share/Media/srt/srtcore/buffer.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/buffer.cpp.o"
  "/root/share/Media/srt/srtcore/cache.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/cache.cpp.o"
  "/root/share/Media/srt/srtcore/channel.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/channel.cpp.o"
  "/root/share/Media/srt/srtcore/common.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/common.cpp.o"
  "/root/share/Media/srt/srtcore/congctl.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/congctl.cpp.o"
  "/root/share/Media/srt/srtcore/core.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/core.cpp.o"
  "/root/share/Media/srt/srtcore/crypto.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/crypto.cpp.o"
  "/root/share/Media/srt/srtcore/epoll.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/epoll.cpp.o"
  "/root/share/Media/srt/srtcore/fec.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/fec.cpp.o"
  "/root/share/Media/srt/srtcore/handshake.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/handshake.cpp.o"
  "/root/share/Media/srt/srtcore/list.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/list.cpp.o"
  "/root/share/Media/srt/srtcore/md5.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/md5.cpp.o"
  "/root/share/Media/srt/srtcore/packet.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/packet.cpp.o"
  "/root/share/Media/srt/srtcore/packetfilter.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/packetfilter.cpp.o"
  "/root/share/Media/srt/srtcore/queue.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/queue.cpp.o"
  "/root/share/Media/srt/srtcore/srt_c_api.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/srt_c_api.cpp.o"
  "/root/share/Media/srt/srtcore/sync.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/sync.cpp.o"
  "/root/share/Media/srt/srtcore/sync_cxx11.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/sync_cxx11.cpp.o"
  "/root/share/Media/srt/srtcore/window.cpp" "/root/share/Media/srt/CMakeFiles/srt_virtual.dir/srtcore/window.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "HAI_ENABLE_SRT=1"
  "HAI_PATCH=1"
  "HAVE_INET_PTON=1"
  "LINUX=1"
  "SRT_DYNAMIC"
  "SRT_ENABLE_APP_READER"
  "SRT_ENABLE_ENCRYPTION"
  "SRT_EXPORTS"
  "SRT_VERSION=\"1.4.1\""
  "USE_OPENSSL=1"
  "_GNU_SOURCE"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "common"
  "srtcore"
  "haicrypt"
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
