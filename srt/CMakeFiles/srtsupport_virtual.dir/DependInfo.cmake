# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/share/Media/srt/apps/apputil.cpp" "/root/share/Media/srt/CMakeFiles/srtsupport_virtual.dir/apps/apputil.cpp.o"
  "/root/share/Media/srt/apps/logsupport.cpp" "/root/share/Media/srt/CMakeFiles/srtsupport_virtual.dir/apps/logsupport.cpp.o"
  "/root/share/Media/srt/apps/socketoptions.cpp" "/root/share/Media/srt/CMakeFiles/srtsupport_virtual.dir/apps/socketoptions.cpp.o"
  "/root/share/Media/srt/apps/transmitmedia.cpp" "/root/share/Media/srt/CMakeFiles/srtsupport_virtual.dir/apps/transmitmedia.cpp.o"
  "/root/share/Media/srt/apps/uriparser.cpp" "/root/share/Media/srt/CMakeFiles/srtsupport_virtual.dir/apps/uriparser.cpp.o"
  "/root/share/Media/srt/apps/verbose.cpp" "/root/share/Media/srt/CMakeFiles/srtsupport_virtual.dir/apps/verbose.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "HAI_ENABLE_SRT=1"
  "HAI_PATCH=1"
  "HAVE_INET_PTON=1"
  "LINUX=1"
  "SRT_ENABLE_APP_READER"
  "SRT_ENABLE_ENCRYPTION"
  "SRT_VERSION=\"1.4.1\""
  "USE_OPENSSL=1"
  "_GNU_SOURCE"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "common"
  "srtcore"
  "haicrypt"
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
