# ffmpeg4.1_h265

#### 介绍
配套SRS_H265使用的ffmpeg，可以支持HEVC（Codec 12）的推流与拉流动作

### libx264

`wget https://johnvansickle.com/ffmpeg/release-source/libx264-git.tar.xz #此步如果拉取本项目则跳过` 

`xz -d libx264-git.tar.xz  #此步如果拉取本项目则跳过`

`tar -xvf libx264-git.tar #此步如果拉取本项目则跳过` 

`cd ./libx264-git`

`./configure --disable-asm --enable-shared`

`make -j 2`

`make install-lib-shared`



### libx265

`yum install cmake`

`wget https://johnvansickle.com/ffmpeg/release-source/libx265-git.tar.xz #此步如果拉取本项目则跳过` 

`xz -d libx265-git.tar.xz #此步如果拉取本项目则跳过` 

`tar -xvf libx265-git.tar #此步如果拉取本项目则跳过` 

`cd libx265-git/source/`

`mkdir build`

`cd build/`

`cmake ..`

`make -j 2`

`make install`



### libfdk_aac

`wget https://downloads.sourceforge.net/opencore-amr/fdk-aac-2.0.1.tar.gz #此步如果拉取本项目则跳过` 

`tar -xzvf fdk-aac-2.0.1.tar.gz #此步如果拉取本项目则跳过` 

`cd fdk-aac-2.0.1`

`./configure`

`make -j 2`

`make install`



### `libsrt`

`yum install git`

`git clone https://github.com/Haivision/srt.git #此步如果拉取本项目则跳过` 

`cd srt`

`git reset --hard 10ed37b6d4b49a3042213b029f0de6bca4bcfe83 #解决srt旧API，在ffmpeg不识别问题，此步如果拉取本项目则跳过`

`./configure`

`make && make install`



### ffmpeg

`wget https://johnvansickle.com/ffmpeg/release-source/ffmpeg-4.1.tar.xz #此步如果拉取本项目则跳过` 

`xz -d ffmpeg-4.1.tar.xz #此步如果拉取本项目则跳过` 

`tar -xvf ffmpeg-4.1.tar #此步如果拉取本项目则跳过` 

`# 获取H265模块支持`

`git clone https://github.com/runner365/ffmpeg_rtmp_h265.git #此步如果拉取本项目则跳过` 

`cp ffmpeg_rtmp_h265/flv.h ffmpeg_rtmp_h265/flvdec.c ffmpeg_rtmp_h265/flvenc.c ffmpeg-4.1/libavformat/ #此步如果拉取本项目则跳过`

`#开始编译`

`cd ffmpeg-4.1`

`export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/usr/local/lib64/pkgconfig`

`./configure --target-os=linux --arch=x86_64 --enable-cross-compile --disable-avdevice --disable-doc --disable-devices --disable-ffplay --enable-libfdk-aac --enable-libx264 --enable-libx265 --enable-libsrt --enable-nonfree --disable-asm --enable-gpl --pkgconfigdir=/usr/local/lib/pkgconfig --enable-shared`

`make -j 2`

`make install`



### ffmpeg 常用指令
##### 引入LD
`export LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib64`
##### 将264流转码保存到ts中
`ffmpeg -i rtmp://192.168.2.102:1935/video/11000000001320002017 -codec copy -bsf:v h264_mp4toannexb OUTPUT.ts`
##### 将265流转码保存到ts中
`ffmpeg -i rtmp://192.168.2.102:1935/video/11000000001320002017 -codec copy -bsf:v hevc_mp4toannexb OUTPUT.ts`

##### RTMP推流（MP4文件）
`ffmpeg -re -stream_loop -1 -i /root/share/h265_high.mp4 -c copy -f flv rtmp://172.17.0.2:1935/video/110000`

PS：

​	a. -re: 代表按照帧率发送,尤其在作为推流工具的时候一定要加入该参数,否则*ffmpeg*会按照最高速率向流媒体服务器不停地发送数据。如果是文件推流必须需要这个参数

​	b. -stream_loop -1: 表示循环推流

##### RTMP推流（RTSP流）
`ffmpeg -rtsp_transport tcp -i rtsp://admin:yhl12345@192.168.2.17:554/Streaming/Channels/101 -c copy -f flv rtmp://172.17.0.2/video/110000`

##### 265转264（文件）
`ffmpeg -i inputfile_265 -map 0 -c:a copy -c:s copy -c:v libx264 output.mkv`

##### 265转264（265-RTSP->264-RTMP）

`ffmpeg -rtsp_transport tcp -hwaccel auto  -i rtsp://admin:yhl12345@130.3.3.211:554/Streaming/Channels/101 -an -vcodec libx264  -threads 10  -bufsize 200m -keyint_min 50 -bf 0  -preset veryfast  -f flv  "rtmp://192.168.182.6:1935/local/test/1"`

PS: 

1. -hwaccel auto:  硬编码支持，可以选择不加
2. -preset veryfast：如果选择fast或更低，实时流可能会出现花屏

#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
